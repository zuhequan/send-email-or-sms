#!/bin/python
# -*- coding: utf-8 -*-

"""
This file is part of web2py Web Framework (Copyrighted, 2007-2010).
Developed by Massimo Di Pierro <mdipierro@cs.depaul.edu>.
License: GPL v2
"""

import thread
import base64
import cPickle
import datetime
from email import *
import logging
import sys
import os
import re
import time
import smtplib
import urllib
import urllib2
import Cookie


class Storage(dict):

    """
    A Storage object is like a dictionary except `obj.foo` can be used
    in addition to `obj['foo']`.

        >>> o = Storage(a=1)
        >>> print o.a
        1

        >>> o['a']
        1

        >>> o.a = 2
        >>> print o['a']
        2

        >>> del o.a
        >>> print o.a
        None

    """

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            return None

    def __setattr__(self, key, value):
        self[key] = value

    def __delattr__(self, key):
        try:
            del self[key]
        except KeyError, k:
            raise AttributeError, k

    def __repr__(self):
        return '<Storage ' + dict.__repr__(self) + '>'

    def __getstate__(self):
        return dict(self)

    def __setstate__(self, value):
        for (k, v) in value.items():
            self[k] = v

def load_storage(filename):
    fp = open(filename, 'rb')
    portalocker.lock(fp, portalocker.LOCK_EX)
    storage = cPickle.load(fp)
    portalocker.unlock(fp)
    fp.close()
    return Storage(storage)


def save_storage(storage, filename):
    fp = open(filename, 'wb')
    portalocker.lock(fp, portalocker.LOCK_EX)
    cPickle.dump(dict(storage), fp)
    portalocker.unlock(fp)
    fp.close()


class Settings(Storage):

    def __setattr__(self, key, value):
        if key != 'lock_keys' and self.get('lock_keys', None)\
             and not key in self:
            raise SyntaxError, 'setting key \'%s\' does not exist' % key
        if key != 'lock_values' and self.get('lock_values', None):
            raise SyntaxError, 'setting value cannot be changed: %s' % key
        self[key] = value



class Mail(object):
    """
    Class for configuring and sending emails with alternative text / html
    body, multiple attachments and encryption support

    Works with SMTP and Google App Engine.
    """

    class Attachment(MIMEBase.MIMEBase):
        """
        Email attachment

        Arguments::

            payload: path to file or file-like object with read() method
            filename: name of the attachment stored in message; if set to
                      None, it will be fetched from payload path; file-like
                      object payload must have explicit filename specified
            content_id: id of the attachment; automatically contained within
                        < and >
            content_type: content type of the attachment; if set to None,
                          it will be fetched from filename using gluon.contenttype
                          module
            encoding: encoding of all strings passed to this function (except
                      attachment body)

        Content ID is used to identify attachments within the html body;
        in example, attached image with content ID 'photo' may be used in
        html message as a source of img tag <img src="cid:photo" />.

        Examples::

            #Create attachment from text file:
            attachment = Mail.Attachment('/path/to/file.txt')

            Content-Type: text/plain
            MIME-Version: 1.0
            Content-Disposition: attachment; filename="file.txt"
            Content-Transfer-Encoding: base64

            SOMEBASE64CONTENT=

            #Create attachment from image file with custom filename and cid:
            attachment = Mail.Attachment('/path/to/file.png',
                                             filename='photo.png',
                                             content_id='photo')

            Content-Type: image/png
            MIME-Version: 1.0
            Content-Disposition: attachment; filename="photo.png"
            Content-Id: <photo>
            Content-Transfer-Encoding: base64

            SOMEOTHERBASE64CONTENT=
        """

        def __init__(
            self,
            payload,
            filename=None,
            content_id=None,
            content_type=None,
            encoding='utf-8'):
            if isinstance(payload, str):
                if filename == None:
                    filename = os.path.basename(payload)
                handle = open(payload, 'rb')
                payload = handle.read()
                handle.close()
            else:
                if filename == None:
                    raise Exception('Missing attachment name')
                payload = payload.read()
            filename = filename.encode(encoding)
            if content_type == None:
                content_type = contenttype(filename)
            MIMEBase.MIMEBase.__init__(self, *content_type.split('/', 1))
            self.set_payload(payload)
            self['Content-Disposition'] = 'attachment; filename="%s"' % filename
            if content_id != None:
                self['Content-Id'] = '<%s>' % content_id.encode(encoding)
            Encoders.encode_base64(self)

    def __init__(self, server=None, sender=None, login=None, tls=True):
        """
        Main Mail object

        Arguments::

            server: SMTP server address in address:port notation
            sender: sender email address
            login: sender login name and password in login:password notation
                   or None if no authentication is required
            tls: enables/disables encryption (True by default)

        In Google App Engine use::

            server='gae'

        For sake of backward compatibility all fields are optional and default
        to None, however, to be able to send emails at least server and sender
        must be specified. They are available under following fields:

            self.settings.server
            self.settings.sender
            self.settings.login

        Examples::

            #Create Mail object with authentication data for remote server:
            mail = Mail('example.com:25', 'me@example.com', 'me:password')
        """

        self.settings = Settings()
        self.settings.server = server
        self.settings.sender = sender
        self.settings.login = login
        self.settings.tls = tls
        self.result = {}
        self.error = None

    def send(
        self,
        to,
        subject='None',
        message='None',
        attachments=None,
        cc=None,
        bcc=None,
        reply_to=None,
        encoding='utf-8'
        ):
        """
        Sends an email using data specified in constructor

        Arguments::

            to: list or tuple of receiver addresses; will also accept single
                object
            subject: subject of the email
            message: email body text; depends on type of passed object:
                     if 2-list or 2-tuple is passed: first element will be
                     source of plain text while second of html text;
                     otherwise: object will be the only source of plain text
                     and html source will be set to None;
                     If text or html source is:
                     None: content part will be ignored,
                     string: content part will be set to it,
                     file-like object: content part will be fetched from
                                       it using it's read() method
            attachments: list or tuple of Mail.Attachment objects; will also
                         accept single object
            cc: list or tuple of carbon copy receiver addresses; will also
                accept single object
            bcc: list or tuple of blind carbon copy receiver addresses; will
                also accept single object
            reply_to: address to which reply should be composed
            encoding: encoding of all strings passed to this method (including
                      message bodies)

        Examples::

            #Send plain text message to single address:
            mail.send('you@example.com',
                      'Message subject',
                      'Plain text body of the message')

            #Send text and html message to three addresses (two in cc):
            mail.send('you@example.com',
                      'Message subject',
                      ('Plain text body', <html>html body</html>),
                      cc=['other1@example.com', 'other2@example.com'])

            #Send html only message with image attachment available from
            the message by 'photo' content id:
            mail.send('you@example.com',
                      'Message subject',
                      (None, '<html><img src="cid:photo" /></html>'),
                      Mail.Attachment('/path/to/photo.jpg'
                                      content_id='photo'))

            #Send email with two attachments and no body text
            mail.send('you@example.com,
                      'Message subject',
                      None,
                      [Mail.Attachment('/path/to/fist.file'),
                       Mail.Attachment('/path/to/second.file')])

        Returns True on success, False on failure.

        Before return, method updates two object's fields:
        self.result: return value of smtplib.SMTP.sendmail() or GAE's
                     mail.send_mail() method
        self.error: Exception message or None if above was successful
        """

        if not isinstance(self.settings.server, str):
            raise Exception('Server address not specified')
        if not isinstance(self.settings.sender, str):
            raise Exception('Sender address not specified')
        if isinstance(to, str):
            to = [to]
        else:
            to = list(to)
        if len(to) == 0:
            raise Exception('Target receiver address not specified')
        payload = MIMEMultipart.MIMEMultipart('related')
        payload['From'] =\
            self.settings.sender.decode(encoding).encode('utf-8')
        payload['To'] = \
            header.Header(', '.join(to).decode(encoding).encode('utf-8'),'utf-8')
        if reply_to != None:
            payload['Reply-To'] = \
                header.Header(reply_to.decode(encoding).encode('utf-8'),'utf-8')
        payload['Subject'] = \
            header.Header(subject.decode(encoding).encode('utf-8'),'utf-8')
        if cc != None:
            if not isinstance(cc, (list, tuple)):
                cc = [cc]
            payload['Cc'] = \
                header.Header(', '.join(cc).decode(encoding).encode('utf-8'),'utf-8')
            to.extend(cc)
        if bcc != None:
            if not isinstance(bcc, (list, tuple)):
                bcc = [bcc]
            payload['Bcc'] = \
                header.Header(', '.join(bcc).decode(encoding).encode('utf-8'),'utf-8')
            to.extend(bcc)
        if message == None:
            text = html = None
        elif isinstance(message, (list, tuple)):
            text, html = message
        else:
            text = message
            html = None
        if text != None or html != None:
            attachment = MIMEMultipart.MIMEMultipart('alternative')
            if text != None:
                if isinstance(text, str):
                    text = text.decode(encoding).encode('utf-8')
                else:
                    text = text.read().decode(encoding).encode('utf-8')
                attachment.attach(MIMEText.MIMEText(text,_charset='utf-8'))
            if html != None:
                if isinstance(html, str):
                    html = html.decode(encoding).encode('utf-8')
                else:
                    html = html.read().decode(encoding).encode('utf-8')
                attachment.attach(MIMEText.MIMEText(html, 'html',_charset='utf-8'))
            payload.attach(attachment)
        if attachments == None:
            pass
        elif isinstance(attachments, (list, tuple)):
            for attachment in attachments:
                payload.attach(attachment)
        else:
            payload.attach(attachments)
        result = {}
        try:
            if self.settings.server == 'gae':
                from google.appengine.api import mail
                result = mail.send_mail(sender=self.settings.sender, to=to,
                                        subject=subject, body=text)
            else:
                server = smtplib.SMTP(*self.settings.server.split(':'))
                if self.settings.login != None:
                    if self.settings.tls:
                        server.ehlo()
                        server.starttls()
                        server.ehlo()
                    server.login(*self.settings.login.split(':'))
                result = server.sendmail(self.settings.sender, to, payload.as_string())
                server.quit()
        except Exception, e:
            logging.warn('Mail.send failure:%s' % e)
            self.result = result
            self.error = e
            return False
        self.result = result
        self.error = None
        return True

        
if __name__ == '__main__':        
    mail = Mail('localhost',sender='publish@monitor.pub.sina.com.cn', tls=False)
    mail.send('huaiyu@staff.sina.com.cn','汉字','汉字')
