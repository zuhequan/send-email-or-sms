#coding:utf-8
import logging
def pyLog():
    conf={}
    conf["name"]="debug"
    conf["filename"]= "/data0/pub/projects/gsps/logs/email_sms.log"
    conf["format"]="[%(asctime)s %(pathname)s]: %(message)s"
    conf["datefmt"]="%Y-%m-%d %H:%M:%S"
    conf["level"]=0
    logger=logging.getLogger(conf["name"])
    handler=logging.FileHandler(conf["filename"])
    fmt = logging.Formatter(conf["format"],conf["datefmt"])
    handler.setFormatter(fmt)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger
