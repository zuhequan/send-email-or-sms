#coding:utf-8
import os

def getProcessList(process):
    pscmd = "ps aux | grep '%s' | grep 'python2.6'"%process
    procs = os.popen(pscmd).read()
    procarr = procs.split("\n")
    rst = []
    for line in procarr:
        if line.strip():
            rst.append(line.split()[1])
    return rst

if __name__ == '__main__':
    getProcessList("python")
