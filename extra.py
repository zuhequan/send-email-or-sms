#coding:utf-8
import sqlite3

sql = """
CREATE TABLE `monitor_alert2` (
  id integer primary key autoincrement,
  `email_content` text,
  `emails` varchar(512) default NULL,
  `phones` varchar(512) default NULL,
  `ctime` datetime default NULL,
  `email_time` datetime default NULL,
  `sms_time` datetime default NULL,
  `email_try_times` int(11) default NULL,
  `sms_try_times` int(11) default NULL,
  `creator` varchar(32) default NULL,
  `status` varchar(6) default 'normal',
  `email_alert_span_minutes` int(11) default '0',
  `sms_alert_span_minutes` int(11) default '0',
  `id_title` varchar(32) default NULL,
  `email_tried_times` int(11) default '0',
  `sms_trid_times` int(11) default '0',
  `email_title` varchar(32) default NULL,
  `remote_ip` varchar(16) default NULL,
  `sms_content` varchar(32) default NULL
)
"""

con = sqlite3.connect('mydatabase.db3')
cur = con.cursor()
cur.execute(sql)
con.commit()