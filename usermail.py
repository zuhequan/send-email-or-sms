#coding:utf-8
#########################
#send mail by smtp
#########################
import MySQLdb
from utils.mail import Mail
import os
class MailSender(object):
    """
    取得需要发送的邮件
    """
    def __init__(self):
        self.log = pyLog()
        self.mailer = Mail("localhost","publish@monitor.pub.sina.com.cn")

    def getCursor(self):
        conn = MySQLdb.connect(db='gsps',port=3506,\
                           host='m3506i.mars.grid.sina.com.cn',\
                           user='publish_w',passwd='ckG83tl53UZpUDne')
        return conn.cursor()

    def sendMails(self):
        querySQL = "SELECT record_id, usermail, title, content FROM \
                    usermail WHERE status='new' ORDER BY record_id ASC";
        self.log.info("get cursor to query")
        cur = self.getCursor()
        self.log.info("get cursor to update")
        update_cur = self.getCursor()
        counts = cur.execute(querySQL)
        rows = cur.fetchall()
        self.log.info("%d new mails to be send"%counts)
        for itm in rows:
            record_id, usermail, title, content = itm
            self.log.info("ready to send mail %d"%record_id)
            if self._send(usermail.strip().split(","), title, content):
                self.log.info("mail %d info,%s,%s,%s"%\
                              (record_id, usermail, title,content))
                self.log.info("send mail %d done"%record_id)
                update_cur.execute("update usermail set status='mail',\
                                    mail_time=NOW() where record_id=%d"%record_id)
                update_cur.connection.commit()
                self.log.info("set  mail %d  status = mail"%record_id)
            else:
                self.log.info("send mail %d failed:%s"%(record_id,str(self.mailer.error)))
        self.log.info("all mail sent this time")
        update_cur.close()
        cur.close()

    def _send(self,recipients,subject,content):
        return self.mailer.send(recipients, subject, [content, content],encoding='gbk')

if __name__ == "__main__":
    import sys
    from utils.processlist import getProcessList
    mail = MailSender()
    existed = getProcessList(sys.argv[0])
    if len(existed)>2:
        mail.log.warning("usermail is running and I exit now")
        sys.exit()
    try:
        mail.sendMails()
    except Exception, e:
        mail.log.error(str(e))
