#coding:utf-8
"""
CREATE TABLE `monitor_alert2` (
  `id` int(11) NOT NULL auto_increment,
  `email_content` text,
  `emails` varchar(512) default NULL,
  `phones` varchar(512) default NULL,
  `ctime` datetime default NULL,
  `email_time` datetime default NULL,
  `sms_time` datetime default NULL,
  `email_try_times` int(11) default NULL,
  `sms_try_times` int(11) default NULL,
  `user` varchar(32) default '',
  `status` varchar(6) default 'normal',
  `email_alert_span_minutes` int(11) default '0',
  `sms_alert_span_minutes` int(11) default '0',
  `id_title` varchar(32) default NULL,
  `email_tried_times` int(11) default '0',
  `sms_tried_times` int(11) default '0',
  `email_title` varchar(32) default NULL,
  `remote_ip` varchar(16) default NULL,
  `sms_content` varchar(1024) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=98712 DEFAULT CHARSET=utf8
"""

from bottle import get, post, request, run
import Queue
import threading
import MySQLdb
import MySQLdb.cursors
from utils.mail import Mail
import urllib2, urllib
import time

mysql_conn = {
    "host":"m3506i.mars.grid.sina.com.cn",
    "user":"publish_w",
    "passwd":"ckG83tl53UZpUDne",
    "port":3506,
    "db":"gsps"
}

# mailer = Mail("localhost","publish@monitor.pub.sina.com.cn")
mailer = Mail("staff.sina.com.cn","huaiyu@staff.sina.com.cn","huaiyu:1132214637", False)
SMS_TASK_QUEUE = Queue.Queue(maxsize = 0)
EMAIL_TASK_QUEUE = Queue.Queue(maxsize = 0)

FORM_FIELDS_NEEDED_DEFAULT_VALUE = {
    "sms_content":"",
    "email_content":"",
    "email_title":"",
    "emails":"",
    "phones":"",
    "email_try_times":5,
    "sms_try_times":5,
    "email_alert_span_minutes":0,
    "sms_alert_span_minutes":0,
    "id_title":"",
    "user":"",
    "coding":"gbk"
}

def query(sql, args=(), as_dict=True):
    conn = MySQLdb.connect(**mysql_conn)
    cur = conn.cursor(MySQLdb.cursors.DictCursor)
    cur.execute(sql, args)
    conn.commit()
    rst = cur.fetchall()
    cur.close()
    conn.close()
    return rst

def getNow():
    conn = MySQLdb.connect(**mysql_conn)
    cur = conn.cursor()
    cur.execute("select NOW()")
    rst = cur.fetchall()[0][0]
    cur.close()
    conn.close()
    return rst

def insert(sql, args = ()):
    conn = MySQLdb.connect(**mysql_conn)
    cur = conn.cursor()
    cur.execute(sql, args)
    conn.commit()
    rst = cur.lastrowid
    cur.close()
    conn.close()
    return rst

def is_less_than_span(t1, t2, maxminutes):
    dt = t1 - t2
    seconds = dt.days * 24 * 3600 + dt.seconds
    return seconds < maxminutes * 60


@get('/get_queue_size')
def get_queue_size():
    return "{'email':%d, 'sms':%d}"%(
                    EMAIL_TASK_QUEUE.qsize(),
                    SMS_TASK_QUEUE.qsize()
                 )

@post('/add_email_sms')
def login_submit():
    received_values = {}
    now = getNow()
    for k in FORM_FIELDS_NEEDED_DEFAULT_VALUE:
        received_values[k] = request.forms.get(k, FORM_FIELDS_NEEDED_DEFAULT_VALUE[k])
        if isinstance(FORM_FIELDS_NEEDED_DEFAULT_VALUE[k], int):
            received_values[k] = int(received_values[k])
    received_values["email_content"] = received_values["email_content"].decode(received_values["coding"]).encode("utf-8")
    received_values["email_title"] = received_values["email_title"].decode(received_values["coding"]).encode("utf-8")
    received_values["sms_content"] = received_values["sms_content"].decode(received_values["coding"]).encode("utf-8")
    received_values["id_title"] = received_values["id_title"].decode(received_values["coding"]).encode("utf-8")
    if not (received_values["phones"] or received_values["emails"]):
        return {"error":"no phones and emails provided"}
    if not received_values["user"]:
        return {"error":"no user name provided"}
    if received_values["email_alert_span_minutes"] and not received_values["id_title"]:
        return {"error":"email_alert_span_minutes provided but withput id_title"}
    if received_values["sms_alert_span_minutes"] and not received_values["id_title"]:
        return {"error":"sms_alert_span_minutes provided but withput id_title"}

    _keys = []
    _values = []
    del received_values["coding"]
    for k in received_values:
        _keys.append(k)
        _values.append(received_values[k])

    _client_ip = request.remote_route
    if _client_ip:
        _client_ip = _client_ip[-1]
    else:
        _client_ip = ""

    _keys += ["ctime", "email_tried_times", "sms_tried_times", "status","remote_ip"]
    _values += [now, 0, 0, "normal", _client_ip]

    _tmp_place_holder = []
    for i in range(len(_values)):
        _tmp_place_holder.append("%s")

    sql = "insert into monitor_alert2(" + ",".join(_keys) + ")values(" + ",".join(_tmp_place_holder) + ")"
    last_row_id = insert(sql, _values)

    phone_sent = 0
    if received_values["phones"]:
        if received_values["id_title"] and \
            received_values["sms_alert_span_minutes"] > 0:
            #发现需要判断是否再次发送短信

            sql = "select id, sms_time from monitor_alert2 where sms_time is not null and user=%s and id_title=%s and phones=%s and id<%s order by id desc limit 1"
            rows = query(sql, (received_values["user"], received_values["id_title"], received_values["phones"], last_row_id))
            if rows:
                #已经有记录，判断上次发送时间
                row = rows[0]
                if not (row["sms_time"] and is_less_than_span(now, row["sms_time"], received_values["sms_alert_span_minutes"])):
                    SMS_TASK_QUEUE.put(last_row_id)
                else:
                    sql = "update monitor_alert2 set sms_time=%s, sms_tried_times=-1 where id=%s"
                    query(sql,(row["sms_time"], last_row_id))
                    phone_sent = 1
            else:
                SMS_TASK_QUEUE.put(last_row_id)
        else:
            SMS_TASK_QUEUE.put(last_row_id)

    email_sent = 0
    if received_values["emails"]:
        if received_values["id_title"] and \
            received_values["email_alert_span_minutes"] > 0:
            #发现需要判断是否再次发送邮件

            sql = "select id, email_time from monitor_alert2 where email_time is not null and  user=%s and id_title=%s and emails=%s and id<%s order by id desc limit 1"
            rows = query(sql, (received_values["user"], received_values["id_title"], received_values["emails"], last_row_id))
            if rows:
                #已经有记录，判断上次发送时间
                row = rows[0]
                if not (row["email_time"] and is_less_than_span(now, row["email_time"], received_values["email_alert_span_minutes"])):
                    EMAIL_TASK_QUEUE.put(last_row_id)
                else:
                    sql = "update monitor_alert2 set email_time=%s, email_tried_times=-1 where id=%s"
                    query(sql,(row["email_time"], last_row_id))
                    email_sent = 1
            else:
                #没有发送过，新建
                EMAIL_TASK_QUEUE.put(last_row_id)
        else:
            EMAIL_TASK_QUEUE.put(last_row_id)
    return "{'success':'1', 'email_send':%d, 'sms_send':%d}"%(email_sent, phone_sent)


class MAIL_QUEUE(threading.Thread):
    def __init__(self, tq):
        self.task_queue = tq
        threading.Thread.__init__(self)

    def eat_queue(self):
        while 1:
            time.sleep(0.01)
            task = self.task_queue.get()
            sql = "select * from monitor_alert2 where id=%s limit 1"
            rows = query(sql, [task])
            if rows:
                row = rows[0]
                recipients = [itm.strip() for itm in row["emails"].split(",")]
                mailer.send(recipients, row['email_title'], [row['email_content'], row['email_content'].replace("\n", "<br />").replace(" ", "&nbsp;")] ,encoding='utf-8')
                if mailer.error:
                    error_times = (row["email_tried_times"] or 0)
                    if error_times < row["email_try_times"]:
                        self.task_queue.put(task)
                        sql = "update monitor_alert2 set email_tried_times=%s where id=%s"
                        query(sql, [error_times+1, task])
                else:
                    sql = "update monitor_alert2 set email_time=NOW(), email_tried_times=0 where id=%s"
                    query(sql, [task])

    def run(self):
        self.eat_queue()


class SMS_QUEUE(threading.Thread):
    def __init__(self, tq):
        self.task_queue = tq
        threading.Thread.__init__(self)

    def eat_queue(self):
        while 1:
            task = self.task_queue.get()
            sql = "select * from monitor_alert2 where id=%s limit 1"
            rows = query(sql, [task])
            if rows:
                row = rows[0]
                recipients = [itm.strip() for itm in row["phones"].split(",")]
                url = "http://mix.sina.com.cn/monitor/Send.php"
                paras = {
                    "user":"publish_dev",
                    "password":"dfea8a2a1d5b981deacb8c1e2c2c540a",
                    "msg":row["sms_content"].decode("utf-8").encode("gbk"),
                    "phone":row["phones"]
                }
                url_paras = urllib.urlencode(paras)
                get_url = url + "?" + url_paras
                req = urllib2.urlopen(get_url)
                rst = req.read(1000)
                req.close()
                if str(rst).strip() != "0":
                    error_times = (row["sms_tried_times"] or 0)
                    if error_times < row["sms_try_times"]:
                        task_queue.put(task)
                        sql = "update monitor_alert2 set sms_tried_times=%s where id=%s"
                        query(sql, [error_times+1, task])
                else:
                    sql = "update monitor_alert2 set sms_time=NOW(), sms_tried_times=0 where id=%s"
                    query(sql, [task])
    def run(self):
        self.eat_queue()

class QueueReputer(threading.Thread):
    """
    当断电等发生时，一旦重启，会将丢失数据起死回生，偶也！
    """
    last_time = 0
    def __init__(self, interval = 15, howmanyminutesago = 10):
        self.interval = interval
        self.howmanyminutesago = howmanyminutesago
        threading.Thread.__init__(self)

    def reload(self):
        while 1:
            seconds = time.time() - QueueReputer.last_time
            if seconds > self.interval:
                print "called me "
                QueueReputer.last_time = time.time()
                # 将满足条件的任务重新放入队列
                # 先检查邮件队列
                sql = "select id from monitor_alert2 where email_try_times < 5 and email_time is null and ctime < date_add(NOW(), interval %d minute) order by id asc limit %d " %(self.howmanyminutesago, self.interval*2)
                rows = query(sql) or []
                for r in rows:
                    EMAIL_TASK_QUEUE.put(r["id"])

                sql = "select id from monitor_alert2 where sms_try_times < 5 and sms_time is null and ctime < date_add(NOW(), interval %d minute) order by id asc limit %d " %(self.howmanyminutesago, self.interval*2)
                rows = query(sql) or []
                for r in rows:
                    SMS_TASK_QUEUE.put(r["id"])

    def run(self):
        self.reload()

thread_mail_queue_consumer = MAIL_QUEUE(EMAIL_TASK_QUEUE)
thread_mail_queue_consumer.start()

thread_sms_queue_consumer = SMS_QUEUE(SMS_TASK_QUEUE)
thread_sms_queue_consumer.start()

thread_queue_reloader = QueueReputer()
thread_queue_reloader.start()

run(host='20.pub.sina.com.cn', port=18080)
